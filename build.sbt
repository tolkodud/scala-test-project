name := "sbt-test"

scalaVersion in Global := "2.11.7"

lazy val commonSettings = Seq(
  parallelExecution in Test := false,
  fork in run := false,
  fork in test := false,
  scalaVersion := "2.11.7"
)

lazy val commonConfig: Project => Project =
  _ settings commonSettings settings (FortyTwoDataBuildPlugin.buildSettings: _*) enablePlugins BuildInfoPlugin

lazy val integrationTestConfig: Project => Project =
  _ configs IntegrationTest settings (Defaults.itSettings: _*)


lazy val root = (project in file("."))
  .configure(commonConfig)
  .settings(FortyTwoDataBuildPlugin.noPublishSettings: _*)
  .aggregate(`test-model`, `test-server`, `test-client`)

lazy val `test-model` = (project in file("model"))

lazy val `test-client` = (project in file("client"))
  .dependsOn(`test-model`)

lazy val `test-server` = (project in file("server"))
  .settings(
    libraryDependencies ++= Seq(
      specs2 % Test
    ),
    dockerBuildExposedPorts := Seq(9000),
    dockerBuildExposedVolumes := Seq.empty
  )
  .configure(commonConfig)
  .settings(FortyTwoDataBuildPlugin.dockerSettings: _*)
  .enablePlugins(DockerPlugin, PlayScala)
  .dependsOn(`test-model`)
