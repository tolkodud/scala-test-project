#!/bin/sh


BUILD_URL="https://napierdrone.eu.ngrok.io/$DRONE_REPO_NAMESPACE/$DRONE_REPO_NAME/$DRONE_BUILD_NUMBER"
case "$1" in 
       "success") 
               BUILD_STATUS="succeeded"
               STATUS_COLOUR="#1ad102"
               STATUS_ICON="&#x2714;"
               ;; 
       "failure") 
               BUILD_STATUS="failed"
               STATUS_COLOUR="#e84809"
               STATUS_ICON="&#x2718;"
               ;; 
esac


cat > pipeline/mail.html <<EOF

<p></p>
<table class="body-wrap" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
<tbody>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
<td class="container" width="600" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
<div class="content" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
<table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
<tbody>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
<td class="alert alert-warning" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: $STATUS_COLOUR; margin: 0; padding: 20px;" align="center" bgcolor="#FF9F00" valign="top"><strong>$STATUS_ICON build $BUILD_STATUS</strong></td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
<td class="content-wrap" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
<tbody>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Please find build metadata below</td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"></tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
<table class="tg" style="undefined;table-layout: fixed; width: 350px">
<tbody>
<tr style="height: 21px;">
<td class="tg-2fdn" style="height: 21px;">BUILD_STARTED</td>
<td class="tg-2fdn" style="height: 21px;">$(date -d @$DRONE_BUILD_STARTED)</td>
</tr>
<tr style="height: 21px;">
<td class="tg-2fdn" style="height: 21px;">BUILD_FINISHED</td>
<td class="tg-2fdn" style="height: 21px;">$(date)</td>
</tr>
<tr style="height: 21px;">
<td class="tg-2fdn" style="height: 21px;">Started by</td>
<td class="tg-2fdn" style="height: 21px;">$DRONE_COMMIT_AUTHOR_NAME</td>
</tr>
<tr style="height: 21px;">
<td class="tg-2fdn" style="height: 21px;">Event Type</td>
<td class="tg-2fdn" style="height: 21px;">$DRONE_BUILD_EVENT</td>
</tr>
<tr style="height: 21px;">
<td class="tg-2fdn" style="height: 21px;">Branch</td>
<td class="tg-2fdn" style="height: 21px;">$DRONE_BRANCH</td>
</tr>
<tr style="height: 21px;">
<td class="tg-2fdn" style="height: 21px;">Commit message</td>
<td class="tg-2fdn" style="height: 21px;">$DRONE_COMMIT_MESSAGE</td>
</tr>
</tbody>
</table>
<p></p>
<p><a href="$BUILD_URL" class="btn-primary" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #fff; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #348eda; margin: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;">View build details</a></p>
</td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"></tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<div class="footer" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;"></div>
</div>
</td>
<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
</tr>
</tbody>
</table>
<p></p>
EOF