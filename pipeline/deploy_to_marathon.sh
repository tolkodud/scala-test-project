#!/bin/sh

set -e

MARATHON_HOST="$1"
MARATHON_URL="http://"$MARATHON_HOST":8080/v2/apps/"
export DOCKER_IMAGE_TAG=$(cat .tags)
export MARATHON_APP_ID=$(cat .app_name)
echo "About to start the deployment of: "$DOCKER_IMAGE_TAG" to "$MARATHON_URL$MARATHON_APP_ID""
echo "Deleting existing app_id: "$MARATHON_APP_ID" from Marathon"


curl -X delete "$MARATHON_URL$MARATHON_APP_ID"
sleep 5
echo "\nCreating new deployment"
envsubst < app.json | curl -H "Content-Type: application/json" -X POST -d @- "$MARATHON_URL?force=true"