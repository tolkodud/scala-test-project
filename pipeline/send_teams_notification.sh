#!/bin/sh


BUILD_URL="https://napierdrone.eu.ngrok.io/$DRONE_REPO_NAMESPACE/$DRONE_REPO_NAME/$DRONE_BUILD_NUMBER"

case "$1" in 
       "success") 
               BUILD_STATUS="succeeded"
               ICON_URL="https://cdn2.iconfinder.com/data/icons/weby-flat-vol-1/512/1_Approved-check-checkbox-confirm-green-success-tick-64.png"
               ;; 
       "failure") 
               BUILD_STATUS="failed"
               ICON_URL="https://cdn0.iconfinder.com/data/icons/super-mono-reflection/red/button-cross_red.png"
               ;; 
esac


generate_post_data()
{
  cat <<EOF

{
    "@type": "MessageCard",
    "@context": "http://schema.org/extensions",
    "themeColor": "0076D7",
    "summary": "$DRONE_REPO_NAME: Build #:$DRONE_BUILD_NUMBER $BUILD_STATUS!",
    "sections": [{
        "activityTitle": "$DRONE_REPO_NAME: Build #:$DRONE_BUILD_NUMBER $BUILD_STATUS!",
        "activitySubtitle": "",
        "activityImage": "$ICON_URL",
        "facts": [{
            "name": "BUILD_STARTED",
            "value": "$(date -d @$DRONE_BUILD_STARTED)"
        },{
            "name": "BUILD_FINISHED",
            "value": "$(date)"
        },{
            "name": "Started by",
            "value": "$DRONE_COMMIT_AUTHOR_NAME"
        },{
            "name": "Event Type",
            "value": "$DRONE_BUILD_EVENT"
        },{
            "name": "Branch",
            "value": "$DRONE_BRANCH"
        },{
            "name": "Commit message",
            "value": "$DRONE_COMMIT_MESSAGE"
        }],
        "markdown": true
    }],
    "potentialAction": [{
        "@type": "OpenUri",
        "name": "View Build",
        "targets": [
            { "os": "default", "uri": "$BUILD_URL" }
        ]
    }]
}

EOF
}

curl \
-H "Content-Type:application/json" \
-X POST --data "$(generate_post_data)" "https://outlook.office.com/webhook/c90f89ef-a42d-4821-9401-1feb0cfb309e@0ba76894-83f2-4b98-aaca-3de49b3e6fa6/IncomingWebhook/f1c7322a707c4e07951ea8580bb59424/900c34b7-c40e-4967-8992-4d0522103d79"
