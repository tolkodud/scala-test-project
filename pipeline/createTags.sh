#!/bin/sh

set -e

# DOCKER_REGISTRY="dev.fortytwodata.com:5007"
# APP_NAME=$(cat .app_name)
APP_VERSION=$(cat version.sbt | grep 'version in ThisBuild :=' | sed 's/.*"\(.*\)".*/\1/')
SHORT_COMMIT_SHA=$(git rev-parse --short HEAD)
# CURRENT_BUILD_BRANCH=$(git rev-parse --abbrev-ref HEAD)

# if [ -z "$DRONE_BUILD_NUMBER" ]; 
# 	then 
# 		echo "DRONE_BUILD_NUMBER is not set as we are running locally. Setting to default value = 0";
# 		DRONE_BUILD_NUMBER=0;
# fi;

# DOCKER_TAG="$DOCKER_REGISTRY"/"$APP_NAME":"$APP_VERSION"."$DRONE_BUILD_NUMBER"_"$CURRENT_BUILD_BRANCH"_"$SHORT_COMMIT_SHA"
# echo "$DOCKER_TAG docker tag has been formed as the result of this build"  
# echo -n "$DOCKER_TAG" > .tags
# echo -n "$APP_NAME" > .app_name

echo "Tagging current commit: $SHORT_COMMIT_SHA with a tag: $APP_VERSION"
git tag "$APP_VERSION" "$SHORT_COMMIT_SHA"
git push origin "$APP_VERSION"