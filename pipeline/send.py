#! /usr/local/bin/python3
"""Sends an actionable message with the mail.html payload
Usage: 'send.py -u <username> -p <password> [-r <recipient>] [-f <paylod file name>]'
"""

import sys
import getopt
from smtplib import SMTP as SMTP
from email.mime.text import MIMEText

SMTP_SERVER = "smtp.office365.com"
SMTP_PORT = 587

def main(argv):
    """The entry point for the script"""
    user = ""
    password = ""
    recipient = ""
    subject = ""
    payload_file = ""


    try:
        opts, _args = getopt.getopt(argv, 'u:p:r:s:f:', ['user=', 'password=', 'recipient=', 'subject=' 'file='])
    except getopt.GetoptError:
        print('send.py -u <username> -p <password> [-r <recipient>]  [-f <paylod file name>]')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-u':
            user = arg
        elif opt == '-p':
            password = arg
        elif opt == '-r':
            recipient = arg
        elif opt == '-s':
            subject = arg
        elif opt == '-f':
            payload_file = arg

    if (not user) or (not password):
        print('send.py -u <username> -p <password> [-r <recipient>] -s <subject>  [-f <paylod file name>]')
        sys.exit(2)

    print('Sending mail from', user)
    send_message(user, password, recipient,subject,  payload_file)

def send_message(user, password, recipient,subject, payload_file):
    """Sends a message from user to self
    Keyword arguments:
    user -- The email address of the user that will send the message
    password -- The password for the user
    recipient -- (Optional)The recipient email address. Default to user
    subject -- The subject of an email.
    """

    if (not recipient):
        recipient = user

    if (not payload_file):
        payload_file = "mail.html"
        
    html_content = ""
    with open(payload_file, 'r') as myfile:
        html_content = myfile.read()

    msg = MIMEText(html_content, 'html')
    msg['Subject'] = subject
    msg['From'] = user

    conn = SMTP(SMTP_SERVER, SMTP_PORT)
    try:
        conn.starttls()
        conn.set_debuglevel(False)
        conn.login(user, password)
        conn.sendmail(user, recipient, msg.as_string())
    finally:
        conn.quit()

    print('Sent the mail')

if __name__ == '__main__':
    main(sys.argv[1:])