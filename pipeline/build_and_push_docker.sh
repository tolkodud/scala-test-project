#!/bin/sh

docker login -u "admin" -p "$DOCKER_PASSWORD" dev.fortytwodata.com:5007 # For pipeline run
echo "About to start docker build and publish steps"
sbt "; set dockerBuildNumber in Global := $DRONE_BUILD_NUMBER; docker:publish"
