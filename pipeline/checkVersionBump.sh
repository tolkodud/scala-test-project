#!/bin/bash


do_compare () {
    if [[ $1 == $2 ]]
    then
        return 1
    fi
    local IFS=.
    local i ver1=($1) ver2=($2)
    # fill empty fields in ver1 with zeros
    for ((i=${#ver1[@]}; i<${#ver2[@]}; i++))
    do
        ver1[i]=0
    done
    for ((i=0; i<${#ver1[@]}; i++))
    do
        if [[ -z ${ver2[i]} ]]
        then
            # fill empty fields in ver2 with zeros
            ver2[i]=0
        fi
        if ((10#${ver1[i]} > 10#${ver2[i]}))
        then
            return 0
        fi
        if ((10#${ver1[i]} < 10#${ver2[i]}))
        then
            return 2
        fi
    done
    return 0
}

show_versions () {
    echo "The current version is --> "$versionInCurrentBranch""
    echo "The version in develop branch is --> "$versionInDevelop""
}

echo "Starting to check if component's version has been incremented"
# versionInDevelop=$(git fetch origin develop:develop && git show develop:version.sbt | grep 'version in ThisBuild :=' | sed 's/.*"\(.*\)".*/\1/')
versionInDevelop=$(git show origin/develop:version.sbt | grep 'version in ThisBuild :=' | sed 's/.*"\(.*\)".*/\1/')
versionInCurrentBranch=$(cat version.sbt | grep 'version in ThisBuild :=' | sed 's/.*"\(.*\)".*/\1/')
do_compare "$versionInCurrentBranch" "$versionInDevelop"

case "$?" in 
       "0") 
               echo "All good. The version has been amended :) "
               show_versions
               echo "Proceeding with the build"
               ;; 
       "1") 
               echo "The version has not been incremented!"
               show_versions
               echo "Please, increment the version. Meanwhile failing the build"
               exit 1
               ;; 
       "2") 
               echo "Wow! It seems the version has been decremented comparing to develop branch!"
               show_versions
               echo "Please, increment the version. Meanwhile failing the build"
               exit 1
               ;; 
esac




