addSbtPlugin("com.fortytwodata" % "build-plugin" % "0.3.0")

//--------------------------------------------------------------------
// Add project specific plugins below
//--------------------------------------------------------------------

addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.5.12")
addSbtPlugin("com.websudos" %% "phantom-sbt" % "1.28.15-f2d")
addSbtPlugin("io.spray" % "sbt-revolver" % "0.7.2")
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")
