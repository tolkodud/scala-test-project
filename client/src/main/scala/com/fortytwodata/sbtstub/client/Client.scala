package com.fortytwodata.sbtstub.client

import com.fortytwodata.sbtstub.model.Model

case class Client(model: Model)
