package controllers

import com.fortytwodata.sbtstub.model.Model
import play.api._
import play.api.mvc._

class Application extends Controller {

  val model = Model(15)

  def index = Action {
    println(model)

    Ok(views.html.index("Your new application is ready."))
  }

}
